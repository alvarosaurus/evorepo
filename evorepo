#! /bin/bash

# Read configuration options
source config.ini

usage() {
	echo "See https://github.com/alvarosaurus/evorepo."
}

matrix() {
	sourcePath=$(realpath ${CSV_FILE})
	destFile=$(realpath ${MATRIX_FILE})
	/usr/bin/Rscript src/DataAnalysis/distance_matrix.R ${sourcePath} ${destFile}

        echo "Saved distance matrix to file ${MATRIX_FILE}"
}

tree() {
	sourcePath=$(realpath ${MATRIX_FILE})
	destFile=$(realpath ${TREEOBJ_FILE})
	/usr/bin/Rscript src/DataAnalysis/tree.R ${sourcePath} ${destFile}

        echo "Saved tree object to file ${TREEOBJ_FILE}"
}

tree_svg() {
	sourcePath=$(realpath ${MATRIX_FILE})
	destFile=$(realpath ${TREEOBJ_FILE})
	/usr/bin/Rscript src/DataAnalysis/tree_svg.R ${sourcePath} ${destFile}

        echo "Saved tree object to file ${TREEOBJ_FILE}"
}

plot() {
	sourcePath=$(realpath ${TREEOBJ_FILE})
	destFile=$(realpath ${TREE_FILE})
	/usr/bin/Rscript src/DataAnalysis/plot.R ${sourcePath} ${destFile}

        echo "Plotted tree to file ${TREE_FILE}"
}

histo() {
	sourcePath=$(realpath ${TREEOBJ_FILE})
	destFile=$(realpath ${HISTO_FILE})
	/usr/bin/Rscript src/DataAnalysis/histo.R ${sourcePath} ${destFile}

        echo "Plotted histogram to file ${HISTO_FILE}"
}

cophenetic() {
	sourcePath=$(realpath ${MATRIX_FILE})
	destFile=$(realpath ${COPH_FILE})
	/usr/bin/Rscript src/DataAnalysis/cophenetic.R ${sourcePath} ${destFile}

        echo "saved cophenetic matrix to file ${COPH_FILE}"
}

case $1 in
    matrix)
        matrix;;
    tree)
        tree;;
    plot)
        plot;;
    histo)
        histo;;
    cophenetic)
        cophenetic;;
    *)
	echo "Unknown command"
	usage;;
esac
