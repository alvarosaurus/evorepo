--  a fork
insert into fork (description) values('test fork');

--  all the projects
insert into project (name, forkId) values('MYSQL', 1 );
insert into project (name, forkId) values('MARIADB', 1 );

--  all the branches
insert into branch (name, projectid) values( 'MYSQL_8.0', 1 );
insert into branch (name, projectid) values( 'MYSQL_5.5', 1 );
-- insert into branch (name, projectid) values( 'MYSQL_5.6', 1 );
-- insert into branch (name, projectid) values( 'MYSQL_5.7', 1 );
-- insert into branch (name, projectid) values( 'MARIADB_5.5', 2 );
-- insert into branch (name, projectid) values( 'MARIADB_10.0', 2 );
-- insert into branch (name, projectid) values( 'MARIADB_10.1', 2 );
-- insert into branch (name, projectid) values( 'MARIADB_10.2', 2 );
-- insert into branch (name, projectid) values( 'MARIADB_10.3', 2 );

--  8 files from 2 branches
insert into file ( path ) values( 'INSTALL' ); --  id 1
insert into file ( path ) values( 'Doxyfile' ); --  id 2
insert into file ( path ) values( 'CMakeLists.txt' ); --  id 3
insert into file ( path ) values( 'config.h.cmake' ); --  id 4
insert into file ( path ) values( 'README' ); --  id 5
insert into file ( path ) values( 'BUILD-CMAKE' ); --  id 6
insert into file ( path ) values( 'INSTALL-WIN-SOURCE' ); --  id 7
insert into file ( path ) values( 'COPYING'); --  id 8

--  these files are only present in branch 1
insert into version ( fileId, size, checksum, branchid ) values( 1, 10, 'f02bf1df8a24a4362533c6812cef159d', 1);
insert into version ( fileId, size, checksum, branchid ) values( 2, 20, '58c6a11c41fda6f52511c9dae1a63d05', 1);
insert into version ( fileId, size, checksum, branchid ) values( 3, 30, 'b95ccb249b8a54deb11b7082c092cc4e', 1);
insert into version ( fileId, size, checksum, branchid ) values( 4, 40, 'ee864400ae67d34a1992c98303550a5f', 1);

--  this file has two versions in two branches
insert into version ( fileId, size, checksum, branchid ) values( 5, 50, '7654d0b504fa7eccb8a89096c2b6be33', 1);
insert into version ( fileId, size, checksum, branchid ) values( 5, 60, 'ac6024087b725ef18ad506b6e76e7748', 2);

--  these files are only present in branch 2
insert into version ( fileId, size, checksum, branchid ) values( 6, 70, '0e5586da898b0011b0ee9ebebcd5a316', 2);
insert into version ( fileId, size, checksum, branchid ) values( 7, 80, '89e99f9e4265f0a180013b79477d2614', 2);

--  these two files have the same checksum
insert into version ( fileId, size, checksum, branchid ) values( 8, 90, '751419260aa954499f7abaabaa882bbe', 1);
insert into version ( fileId, size, checksum, branchid ) values( 8, 90, '751419260aa954499f7abaabaa882bbe', 2);

--  3 contributors
--  'Tester001 Test' => 8187b2c9de9044e3732f9a7ee0c37254
--  'Tester002 Test' => 85c43dbfe85b8a2ba401998f01cf5a0c
--  'Tester003 Test' => dd965dc641052ef709c68dc9238191e6
insert into contributor (nickname) values('Tester001 Test');
insert into contributor (nickname) values('Tester002 Test');
insert into contributor (nickname) values('Tester003 Test');

--  Contributor 1 added 1 edit to branches 1 and 2
insert into contribution (contributorId, branchId, total, type) values(1, 1, 1, 'EDIT');
insert into contribution (contributorId, branchId, total, type) values(1, 2, 1, 'EDIT');

--  Contributor 2 added 1 edit to branch 2
insert into contribution (contributorId, branchId, total, type) values(2, 2, 1, 'EDIT');

--  Contributor 3 added 1 commit to branch 1
insert into contribution (contributorId, branchId, total, type) values(1, 1, 1, 'COMMIT');
