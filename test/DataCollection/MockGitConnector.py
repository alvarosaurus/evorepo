from evorepo.GitConnector import AGitConnector


class MockGitConnector(AGitConnector):

    testLogPath = "test/DataCollection/testdata/gittest.log"
    authorLogPath = "test/DataCollection/testdata/gitauthors.log"

    def __init__(self):
        pass

    def getAuthorLog(self, repositoryPath, startTag, endTag):
        data = None
        with open(MockGitConnector.authorLogPath, 'r') as logFile:
            data = logFile.read()
        # the author log has 1 line per author
        nameDict = AGitConnector.parseContributorLog(data)
        return nameDict

    def getCommitterLog(self, repositoryPath, startTag, endTag):
        data = None
        with open(MockGitConnector.authorLogPath, 'r') as logFile:
            data = logFile.read()
        # the author log has 1 line per author
        nameDict = AGitConnector.parseContributorLog(data)
        return nameDict
