##
# python3 -m unittest test.RepositoryTest.RepositoryTest

import unittest
import os
from evorepo.Repository import Repository
from evorepo.Postgres import PgDatabase, PgDAOFactory
from evorepo.Model import Project, Branch, Version


class RepositoryTest(unittest.TestCase):
    """
    Run this test on the test tree in testdata.
    """
    dbName = "test"
    factory = None
    db = None

    rootPath = "test/DataCollection/testdata/testtree"

    def setUp(self):
        self.db = PgDatabase(self.dbName).create()
        # create a project for the branch
        self.project = Project(name="test project")
        # create a branch for the file
        self.branch = Branch(name="test branch", projectId=self.project.id)
        self.factory = PgDAOFactory(self.dbName)

    def test_list(self):
        repo = Repository(self.project, self.branch, self.rootPath)
        repo.list()
        paths = []
        for file in repo.fileList:
            paths.append(file.path)
        self.assertEqual(3, len(repo.fileList))
        self.assertTrue("test1.txt" in paths)
        self.assertTrue("test2.txt" in paths)
        self.assertTrue("/sub1/test1.1.txt" in paths)

    def test_checksum(self):
        searchPath = os.path.join(self.rootPath, "test1.txt")
        self.assertEqual(
                "3e7705498e8be60520841409ebc69bc1",
                Version.getChecksum(searchPath))

    def tearDown(self):
        self.db.drop()


if __name__ == '__main__':
    unittest.main()
