##
# python3 -m unittest test.DatabaseTest.DatabaseTest
# or
# python  test/DatabaseTest.py

import unittest
from evorepo.Model import Project, Fork, Branch, File, Version, Contributor, Contribution
from evorepo.Postgres import PgDatabase, PgDAOFactory


class DatabaseTest(unittest.TestCase):
    """
    Run this test with a user that has DATABASE CREATION PRIVILEGES.
    """
    dbName = "test"
    factory = None
    db = None

    def setUp(self):
        """
        Test that createTables.sql is correct.
        """
        self.db = PgDatabase(self.dbName).create()
        self.factory = PgDAOFactory(self.dbName)

    def test_fork(self):
        """
        Write and read a Fork object
        """
        # write
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()

        # read
        new = self.factory.getForkDAO().read(fork.id) 

        self.assertEqual(fork.id, new.id)
        self.assertEqual(fork.description, new.description)

    def test_sameFork(self):
        """
        Save two forks with the same description results in one Fork object
        """
        # write
        fork1 = Fork(description="test fork")
        fork1 = self.factory.getForkDAO(fork1).save()
        fork2 = Fork(description="test fork")
        fork2 = self.factory.getForkDAO(fork2).save()

        self.assertEqual(fork1.id, fork2.id)

    def test_findFork(self):
        """
        Save two forks and find them by description
        """
        # write
        fork1 = Fork(description="test fork 1")
        fork1 = self.factory.getForkDAO(fork1).save()
        fork2 = Fork(description="test fork 2")
        fork2 = self.factory.getForkDAO(fork2).save()

        self.assertEqual(fork1.id, self.factory.getForkDAO().find("test fork 1").id)
        self.assertEqual(fork2.id, self.factory.getForkDAO().find("test fork 2").id)

    def test_project(self):
        """
        Write and read a Project object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()

        # write
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # read
        new = self.factory.getProjectDAO().read(project.id)

        self.assertEqual(project.id, new.id)
        self.assertEqual(project.name, new.name)

    def test_branch(self):
        """
        Write and read a Branch object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()

        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # write
        branch = Branch(name="test branch", projectId=project.id)
        branch = self.factory.getBranchDAO(branch).save()

        # read
        new = self.factory.getBranchDAO().read(branch.id)

        self.assertEqual(branch.id, new.id)
        self.assertEqual(branch.name, new.name)
        self.assertEqual(project.id, new.projectId)

    def test_2project(self):
        """
        Create two projects with different names.
        Expect them to have different project.id
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()

        # create a project for the branch
        project1 = Project(name="test project 1", forkId=fork.id)
        project1 = self.factory.getProjectDAO(project1).save()
        # create a project with the same name
        project2=Project(name="test project 2", forkId=fork.id)
        project2 = self.factory.getProjectDAO(project2).save()

        self.assertNotEqual(project1.id, project2.id)

    def test_1project(self):
        """
        Create a project then try to save a new project by the same name.
        Expect it to return the same project.id
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project1 = Project(name="test project", forkId=fork.id)
        project1 = self.factory.getProjectDAO(project1).save()
        # create a project with the same name
        project2 = Project(name="test project")
        project2 = self.factory.getProjectDAO(project2).save()

        self.assertEqual(project1.id, project2.id)

    def test_2branches(self):
        """
        Create two branches with different names in the same project.
        Expect them to have different branch.id
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # write
        branch1 = Branch(name="test branch 1", projectId=project.id)
        branch1 = self.factory.getBranchDAO(branch1).save()
        branch2 = Branch(name="test branch 2", projectId=project.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        self.assertNotEqual(branch1.id, branch2.id)

    def test_2branch_1project(self):
        """
        Create a branch then try to save a new branch by the same name in the same project.
        Expect it to return the same branch.id
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # write
        branch1 = Branch(name="test branch", projectId=project.id)
        branch1 = self.factory.getBranchDAO(branch1).save()
        branch2 = Branch(name="test branch", projectId=project.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        self.assertEqual(branch1.id, branch2.id)

    def test_2branch_2projects(self):
        """
        Create a branch then try to save a new branch by the same name in another project.
        Expect it to return a different branch.id
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the first branch
        project1 = Project(name="test project 1", forkId=fork.id)
        project1 = self.factory.getProjectDAO(project1).save()

        # write the first branch
        branch1 = Branch(name="test branch", projectId=project1.id)
        branch1 = self.factory.getBranchDAO(branch1).save()

        # create a project for the second branch
        project2 = Project(name="test project 2", forkId=fork.id)
        project2 = self.factory.getProjectDAO(project2).save()

        # write the second branch
        branch2 = Branch(name="test branch", projectId=project2.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        self.assertNotEqual(branch1.id, branch2.id)

    def test_listBranchByFork(self):
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()

        # create a project for the first branch
        project1 = Project(name="test project 1", forkId=fork.id)
        project1 = self.factory.getProjectDAO(project1).save()

        branch1 = Branch(name="test branch 1", projectId=project1.id)
        branch1 = self.factory.getBranchDAO(branch1).save()

        # create a project for the second branch
        project2 = Project(name="test project 2", forkId=fork.id)
        project2 = self.factory.getProjectDAO(project2).save()
        branch2 = Branch(name="test branch 2", projectId=project2.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        # a branch from an unrelated fork should not be listed
        fork2 = Fork(description="test fork 2")
        fork2 = self.factory.getForkDAO(fork2).save()
        project3 = Project(name="test project 3", forkId=fork2.id)
        project3 = self.factory.getProjectDAO(project3).save()
        branch3 = Branch(name="test branch 2", projectId=project3.id)
        branch3 = self.factory.getBranchDAO(branch3).save()

        branchDAO = self.factory.getBranchDAO()
        self.assertEquals(2, len(branchDAO.listByFork(fork.id)))

    def test_file(self):
        """
        Write and read a File object
        """
        # write
        filePath = "test/testdata/testtree/test1.txt"
        file = File(path=filePath)
        file = self.factory.getFileDAO(file).save()

        # read
        new = self.factory.getFileDAO().read(file.id)

        self.assertEqual(file.id, new.id)
        self.assertEqual(file.path, new.path)

    def test_Contributor(self):
        """
        Write and read a Contributor object
        """
        # write
        nickname = "testContributor"
        ctbr = Contributor(nickname=nickname)
        ctbr = self.factory.getContributorDAO(ctbr).save()

        # read
        new = self.factory.getContributorDAO().read(ctbr.id)

        self.assertEqual(ctbr.id, new.id)
        self.assertEqual(ctbr.nickname, new.nickname)

    def test_listContributor(self):
        """
        List Contributor objects
        """
        # write
        nickname1 = "testContributor1"
        ctbr1 = Contributor(nickname=nickname1)
        ctbr1 = self.factory.getContributorDAO(ctbr1).save()
        nickname2 = "testContributor2"
        ctbr2 = Contributor(nickname=nickname2)
        ctbr2 = self.factory.getContributorDAO(ctbr2).save()

        # list
        self.assertEqual(2, len(self.factory.getContributorDAO().list()))
        self.assertEqual(ctbr1.nickname,
                         self.factory.getContributorDAO().list()[0].nickname)

    def test_Edit(self):
        """
        Write and read an Edit object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # create a branch for the edit
        branch = Branch(name="test branch", projectId=project.id)
        branch = self.factory.getBranchDAO(branch).save()

        # create a contributor for the edit
        ctbr = Contributor(nickname="test author")
        ctbr = self.factory.getContributorDAO(ctbr).save()

        # write
        testcount = 3
        edit = Contribution(contributorId=ctbr.id, branchId=branch.id, total=testcount, type=Contribution.EDIT)
        edit = self.factory.getContributionDAO(edit).save()

        # read
        new = self.factory.getContributionDAO().read(edit.id)

        self.assertEqual(edit.id, new.id)
        self.assertEqual(edit.contributorId, new.contributorId)
        self.assertEqual(edit.branchId, new.branchId)
        # time zones may have been changed by Postgres, but the date needs to be the same
        self.assertEqual(edit.total, new.total)

    def test_updateEdit(self):
        """
        Write and update an Edit object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # create a branch for the edit
        branch = Branch(name="test branch", projectId=project.id)
        branch = self.factory.getBranchDAO(branch).save()

        # create a Contributor for the edit
        ctbr = Contributor(nickname="test author")
        ctbr = self.factory.getContributorDAO(ctbr).save()

        # write
        testcount1 = 3
        edit = Contribution(contributorId=ctbr.id, branchId=branch.id, total=testcount1, type=Contribution.EDIT)
        edit = self.factory.getContributionDAO(edit).save()

        # update
        testcount2 = 4
        edit.total = testcount2
        edit = self.factory.getContributionDAO(edit).update()

        # read
        new = self.factory.getContributionDAO().read(edit.id)
        self.assertEqual(testcount2, new.total)

    def test_listEdits(self):
        """
        Write and read an Edit object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # create a branch for the edit
        branch1 = Branch(name="test branch1", projectId=project.id)
        branch1=self.factory.getBranchDAO(branch1).save()
        branch2=Branch(name="test branch2", projectId=project.id)
        branch2=self.factory.getBranchDAO(branch2).save()
        
        # create a Contributor for the edit
        ctbr1=Contributor(nickname="test author1")
        ctbr1=self.factory.getContributorDAO(ctbr1).save()
        ctbr2=Contributor(nickname="test author2")
        ctbr2=self.factory.getContributorDAO(ctbr2).save()
        
        # write
        edit1=Contribution(contributorId=ctbr1.id, branchId=branch1.id, type=Contribution.EDIT)
        edit1=self.factory.getContributionDAO(edit1).save()
        edit2=Contribution(contributorId=ctbr1.id, branchId=branch2.id, type=Contribution.EDIT)
        edit2=self.factory.getContributionDAO(edit2).save()
        edit3=Contribution(contributorId=ctbr2.id, branchId=branch1.id, type=Contribution.EDIT)
        edit3=self.factory.getContributionDAO(edit3).save()
        edit4=Contribution(contributorId=ctbr2.id, branchId=branch2.id, type=Contribution.EDIT)
        edit4=self.factory.getContributionDAO(edit4).save()
        
        # list all
        all=self.factory.getContributionDAO().list()
        self.assertEquals(4, len(all))
        # list all by file branch and contributor
        forbranch1ctbr1=self.factory.getContributionDAO().list(requestedContributorId=ctbr1.id, requestedBranchId=branch1.id, type=Contribution.EDIT)
        self.assertEquals(1, len(forbranch1ctbr1))
        
    def test_Commits(self):
        """
        Write and read a Commit object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()
        
        # create a branch for the edit
        branch=Branch(name="test branch", projectId=project.id)
        branch=self.factory.getBranchDAO(branch).save()
        
        # create a contributor for the edit
        ctbr=Contributor(nickname="test author")
        ctbr=self.factory.getContributorDAO(ctbr).save()
        
        # write
        testcount=3
        commit=Contribution(contributorId=ctbr.id, branchId=branch.id, total=testcount, type=Contribution.COMMIT)
        commit=self.factory.getContributionDAO(commit).save()
        
        # read
        new=self.factory.getContributionDAO().read(commit.id)

        self.assertEqual(commit.id, new.id)
        self.assertEqual(commit.contributorId, new.contributorId)
        self.assertEqual(commit.branchId, new.branchId)
        # time zones may have been changed by Postgres, but the date needs to be the same
        self.assertEqual(commit.total, new.total)

    def test_updateCommits(self):
        """
        Write and update a Commit object
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()
        
        # create a branch for the edit
        branch=Branch(name="test branch", projectId=project.id)
        branch=self.factory.getBranchDAO(branch).save()
        
        # create a Contributor for the edit
        ctbr=Contributor(nickname="test author")
        ctbr=self.factory.getContributorDAO(ctbr).save()
        
        # write
        testcount1=3
        commit=Contribution(contributorId=ctbr.id, branchId=branch.id, total=testcount1, type=Contribution.COMMIT)
        commit=self.factory.getContributionDAO(commit).save()
        
        # update
        testcount2=4
        commit.total=testcount2
        commit=self.factory.getContributionDAO(commit).update()

        # read
        new=self.factory.getContributionDAO().read(commit.id)
        self.assertEqual(testcount2, new.total)

    def test_listCommits(self):
        """
        List Commit objects
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()
        
        # create a branch for the edit
        branch1 = Branch(name="test branch1", projectId=project.id)
        branch1 = self.factory.getBranchDAO(branch1).save()
        branch2 = Branch(name="test branch2", projectId=project.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        # create a Contributor for the edit
        ctbr1 = Contributor(nickname="test author1")
        ctbr1 = self.factory.getContributorDAO(ctbr1).save()
        ctbr2 = Contributor(nickname="test author2")
        ctbr2 = self.factory.getContributorDAO(ctbr2).save()

        # write
        commit1 = Contribution(contributorId=ctbr1.id, branchId=branch1.id, type=Contribution.COMMIT)
        commit1 = self.factory.getContributionDAO(commit1).save()
        commit2 = Contribution(contributorId=ctbr1.id, branchId=branch2.id, type=Contribution.COMMIT)
        commit2 = self.factory.getContributionDAO(commit2).save()
        commit3 = Contribution(contributorId=ctbr2.id, branchId=branch1.id, type=Contribution.COMMIT)
        commit3 = self.factory.getContributionDAO(commit3).save()
        commit4 = Contribution(contributorId=ctbr2.id, branchId=branch2.id, type=Contribution.COMMIT)
        commit4 = self.factory.getContributionDAO(commit4).save()

        # list all
        all = self.factory.getContributionDAO().list(type=Contribution.COMMIT)
        self.assertEquals(4, len(all))
        # list all by file branch and contributor
        forbranch1ctbr1 = self.factory.getContributionDAO().list(requestedContributorId=ctbr1.id, requestedBranchId=branch1.id, type=Contribution.COMMIT)
        self.assertEquals(1, len(forbranch1ctbr1))

    def test_listProjects(self):
        """
        List all projects in a db
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create projects
        project1 = Project(name="test project 1", forkId=fork.id)
        project1 = self.factory.getProjectDAO(project1).save()
        project2 = Project(name="test project 2", forkId=fork.id)
        project2 = self.factory.getProjectDAO(project2).save()

        # list
        projectList = self.factory.getProjectDAO().list()
        self.assertEqual(2, len(projectList))

    def test_listBranches(self):
        """
        List all branches in a project
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project1 = Project(name="test project", forkId=fork.id)
        project1 = self.factory.getProjectDAO(project1).save()

        # create branches
        branch1 = Branch(name="test branch 1", projectId=project1.id)
        branch1 = self.factory.getBranchDAO(branch1).save()
        branch2 = Branch(name="test branch 2", projectId=project1.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        # add a third branch for another project
        project2 = Project(name="test project 2", forkId=fork.id)
        project2 = self.factory.getProjectDAO(project2).save()
        branch3 = Branch(name="test branch 2", projectId=project2.id)
        branch3 = self.factory.getBranchDAO(branch3).save()

        # list branches for project 1
        branchList = self.factory.getBranchDAO().list(project1.id)
        self.assertEqual(2, len(branchList))

        # list all branches for all projects
        branchList = self.factory.getBranchDAO().list()
        self.assertEqual(3, len(branchList))

    def test_listVersions(self):
        """
        List all versions of a file in different branches
        """
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # create a branch for the file
        branch1 = Branch(name="test branch 1", projectId=project.id)
        branch1 = self.factory.getBranchDAO(branch1).save()

        # create a branch for the file
        branch2 = Branch(name="test branch 2", projectId=project.id)
        branch2 = self.factory.getBranchDAO(branch2).save()

        # create a path
        fileUrl = "test/testdata/testtree/test1.txt"
        file = File(path=fileUrl)
        file = self.factory.getFileDAO(file).save()

        # write
        version1 = Version(fileId=file.id, branchId=branch1.id)
        version1.checksum = Version.getChecksum(fileUrl)
        version1 = self.factory.getVersionDAO(version1).save()

        version2 = Version(fileId=file.id, branchId=branch2.id)
        version2.checksum = Version.getChecksum(fileUrl)
        version2 = self.factory.getVersionDAO(version2).save()

        # list
        versionList = self.factory.getVersionDAO().listVersionsByPath(file.id)
        self.assertEqual(2, len(versionList))

    def test_checksum(self):
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # create a branch for the file
        branch = Branch(name="test branch 1", projectId=project.id)
        branch = self.factory.getBranchDAO(branch).save()

        # create a path
        fileUrl = "test/testdata/testtree/test1.txt"
        file = File(path=fileUrl)
        file = self.factory.getFileDAO(file).save()

        # write
        version = Version(fileId=file.id, branchId=branch.id)
        version.checksum = Version.getChecksum(fileUrl)
        version = self.factory.getVersionDAO(version).save()

        # read
        list = self.factory.getVersionDAO(version).listVersionsByPath(requestedFileId=file.id, requestedBranchId=branch.id)
        self.assertEqual(1, len(list))
        version = list[0]
        self.assertEqual(Version.getChecksum(fileUrl), version.checksum)

    def test_size(self):
        # write a fork
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        # create a project for the branch
        project = Project(name="test project", forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()

        # create a branch for the file
        branch = Branch(name="test branch 1", projectId=project.id)
        branch = self.factory.getBranchDAO(branch).save()

        # create a path
        fileUrl = "test/testdata/testtree/test1.txt"
        file = File(path=fileUrl)
        file = self.factory.getFileDAO(file).save()

        # write
        version = Version(fileId=file.id, branchId=branch.id)
        version.size = Version.getSize(fileUrl)
        version = self.factory.getVersionDAO(version).save()

        # read
        list = self.factory.getVersionDAO(version).listVersionsByPath(requestedFileId=file.id, requestedBranchId=branch.id)
        self.assertEqual(1, len(list))
        version = list[0]
        self.assertEqual(Version.getSize(fileUrl), version.size)

    def test_CtbrlistByFork(self):
        # write a fork
        fork1 = Fork(description="test fork 1")
        fork1 = self.factory.getForkDAO(fork1).save()
        # create 2 projects for the fork
        project1 = Project(name="test project 1", forkId=fork1.id)
        project1 = self.factory.getProjectDAO(project1).save()
        project2 = Project(name="test project 2", forkId=fork1.id)
        project2 = self.factory.getProjectDAO(project2).save()

        # write a second fork
        fork2 = Fork(description="test fork 2")
        fork2 = self.factory.getForkDAO(fork2).save()
        # create 1 project not in the fork
        project3 = Project(name="test project 3", forkId=fork2.id)
        project3 = self.factory.getProjectDAO(project3).save()

        # create 2 branches in each project in the fork
        branch11 = Branch(name="test branch 11", projectId=project1.id)
        branch11 = self.factory.getBranchDAO(branch11).save()
        branch12 = Branch(name="test branch 12", projectId=project1.id)
        branch12 = self.factory.getBranchDAO(branch12).save()
        branch21 = Branch(name="test branch 21", projectId=project1.id)
        branch21 = self.factory.getBranchDAO(branch21).save()
        branch22 = Branch(name="test branch 22", projectId=project1.id)
        branch22 = self.factory.getBranchDAO(branch22).save()

        # create 1 branch not in the fork
        branch31 = Branch(name="test branch 31", projectId=project3.id)
        branch31 = self.factory.getBranchDAO(branch31).save()

        # create 3 Contributors
        ctbr1 = Contributor(nickname="test author1")
        ctbr1 = self.factory.getContributorDAO(ctbr1).save()
        ctbr2 = Contributor(nickname="test author2")
        ctbr2 = self.factory.getContributorDAO(ctbr2).save()
        ctbr3 = Contributor(nickname="test author3")
        ctbr3 = self.factory.getContributorDAO(ctbr3).save()

        # ctbr1 commits and edits in 2 branches in the fork
        edit1 = Contribution(contributorId=ctbr1.id, branchId=branch11.id, type=Contribution.EDIT)
        edit1 = self.factory.getContributionDAO(edit1).save()
        commit1 = Contribution(contributorId=ctbr1.id, branchId=branch21.id, type=Contribution.COMMIT)
        commit1 = self.factory.getContributionDAO(commit1).save()

        # ctbr2 edits only branch2 in the fork
        edit2 = Contribution(contributorId=ctbr2.id, branchId=branch21.id, type=Contribution.EDIT)
        edit2 = self.factory.getContributionDAO(edit2).save()

        # ctbr3 edits only outside the fork
        edit3 = Contribution(contributorId=ctbr3.id, branchId=branch31.id, type=Contribution.EDIT)
        edit3 = self.factory.getContributionDAO(edit3).save()

        # list ctbrs for fork lists both ctbrs in the fork
        ctbrDAO = self.factory.getContributorDAO()
        self.assertEquals(2, len(ctbrDAO.listByFork(fork1.id)))

    def tearDown(self):
        self.db.drop()


if __name__ == '__main__':
    unittest.main()
