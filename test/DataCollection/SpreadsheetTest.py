##
# python3 -m unittest test.SpreadsheetTest.SpreadsheetTest
# or
# python test/SpreadsheetTest.py

import unittest
import psycopg2
import os
from evorepo.Postgres import PgDatabase, PgDAOFactory
from evorepo.Spreadsheet import Spreadsheet


class SpreadsheetTest(unittest.TestCase):
    """
    Run this test with a user that has DATABASE CREATION PRIVILEGES.
    """
    dbName = "test"
    db = None
    testdb = os.path.join(os.getcwd(), "test", "DataCollection", "testdata", "testdb.sql")
    testFile = "/tmp/output.csv"
    reportFile = "/tmp/output_report.txt"

    def setUp(self):
        """
        Create test db and load test data
        """
        self.db = PgDatabase(self.dbName).create()
        self.db.load(SpreadsheetTest.testdb)
        self.factory = PgDAOFactory(self.dbName)

    def test_dataLoaded(self):
        # count projects
        response = self._execute(self.dbName, "select count(*) from project")
        self.assertEqual(2, response[0])
        # count branches
        response = self._execute(self.dbName, "select count(*) from branch")
        self.assertEqual(2, response[0])
        # count files
        response = self._execute(self.dbName, "select count(*) from file")
        self.assertEqual(8, response[0])
        # count contributors
        response = self._execute(
            self.dbName,
            "select count(*) from contributor")
        self.assertEqual(3, response[0])
        # count edits
        response = self._execute(
            self.dbName,
            "select count(*) from contribution where type='EDIT'")
        self.assertEqual(3, response[0])
        # count commits
        response = self._execute(
            self.dbName,
            "select count(*) from contribution where type='COMMIT'")
        self.assertEqual(1, response[0])

    def test_report(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        print(sheet.asString())
        self.assertEqual(8, sheet.report['file_present'])
        self.assertEqual(2, sheet.report['file_checksum'])
        self.assertEqual(2, sheet.report['file_size'])
        # there are 3 contributors in the test database,
        # but 1 did not commit or edit
        self.assertEqual(2, sheet.report['ctbr_present'])
        self.assertEqual(2, sheet.report['ctbr_edit'])
        self.assertEqual(2, sheet.report['ctbr_commit'])

        self.assertEqual(8, sheet.report['file_present_effective'])
        self.assertEqual(2, sheet.report['file_checksum_effective'])
        self.assertEqual(2, sheet.report['file_size_effective'])
        self.assertEqual(2, sheet.report['ctbr_present_effective'])
        self.assertEqual(2, sheet.report['ctbr_edit_effective'])
        self.assertEqual(2, sheet.report['ctbr_commit_effective'])

    def _getSet(self, sheet, fileName):
        found = {'present': None, 'checksum': None, 'size': None}
        for row in sheet.rows:
            if row.name == fileName:
                found['present'] = row

            elif row.name == ("Checksum %s" % fileName):
                found['checksum'] = row

            elif row.name == ("Size %s" % fileName):
                found['size'] = row

        return found

    def _getContributor(self, sheet, nickname):
        found = {'present': None, 'edit': None, 'commit': None}
        for row in sheet.rows:
            if row.name == nickname:
                found['present'] = row

            elif row.name == ("Edits %s" % nickname):
                found['edit'] = row

            elif row.name == ("Commits %s" % nickname):
                found['commit'] = row

        return found

    def test_RowCount(self):
        # there are 18 effective rows in the test spreadsheet
        sheet = Spreadsheet(self.factory, forkId=1).export()
        self.assertEqual(18, len(sheet.rows))

        # all characteristics
        sheet = Spreadsheet(self.factory, forkId=1).export(Spreadsheet.ALL)
        self.assertEqual(18, len(sheet.rows))

        # code characteristics
        sheet = Spreadsheet(self.factory, forkId=1).export(Spreadsheet.CODE)
        self.assertEqual(12, len(sheet.rows))

        # team characteristics
        sheet = Spreadsheet(self.factory, forkId=1).export(Spreadsheet.TEAM)
        self.assertEqual(6, len(sheet.rows))

    def test_exportPresentFiles(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        set1 = self._getSet(sheet, "BUILD-CMAKE")
        self.assertEqual("BUILD-CMAKE", set1['present'].name)
        self.assertEqual('FALSE', set1['present'].branches[1])
        self.assertEqual('TRUE', set1['present'].branches[2])

    def test_exportChecksums(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()

        # This file is present in only one branch, so its
        # checksum will not be included in the import.
        set1 = self._getSet(sheet, "BUILD-CMAKE")
        self.assertFalse(set1['checksum'])

        # this file is present in branch 1 and 2 with different checksums
        set2 = self._getSet(sheet, "README")
        self.assertEqual("Checksum README", set2['checksum'].name)
        # checksum
        # '7654d0b504fa7eccb8a89096c2b6be33'='AA',
        # 'ac6024087b725ef18ad506b6e76e7748'='AB'
        self.assertEqual('AA', set2['checksum'].branches[1])
        self.assertEqual('AB', set2['checksum'].branches[2])

        # This file is present in both branches, so its
        # checksum will be included in the import.
        set3 = self._getSet(sheet, "COPYING")
        self.assertTrue(set3['checksum'])
        self.assertEqual('AA', set3['checksum'].branches[1])
        self.assertEqual('AA', set3['checksum'].branches[2])

    def test_exportSizes(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        set1 = self._getSet(sheet, "BUILD-CMAKE")

        # this file is only present in branch 2
        self.assertFalse(set1['size'])

        # this file has the same checksum in two branches
        set2 = self._getSet(sheet, "COPYING")
        self.assertTrue(set2['size'])

        # this file is present in branch 1 and 2 with different checksums
        set3 = self._getSet(sheet, "README")
        self.assertEqual("Size README", set3['size'].name)
        # branch 1; 50, branch2: 60
        self.assertEqual(50, set3['size'].branches[1])
        self.assertEqual(60, set3['size'].branches[2])

    def test_exportPresent(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        set2 = self._getContributor(sheet, "Tester002 Test")
        self.assertEqual('FALSE', set2['present'].branches[1])
        self.assertEqual('TRUE', set2['present'].branches[2])

    def test_exportEdits(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        set1 = self._getContributor(sheet, "Tester002 Test")

        self.assertEqual(0, set1['edit'].branches[1])
        self.assertEqual(1, set1['edit'].branches[2])

    def test_exportCommits(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        set1 = self._getContributor(sheet, "Tester001 Test")
        # tester01 edited branches 1 and 2
        self.assertEqual(1, set1['commit'].branches[1])
        self.assertEqual(0, set1['commit'].branches[2])

    def test_canWriteReport(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        sheet.csv(SpreadsheetTest.testFile)
        self.assertTrue(os.path.isfile(SpreadsheetTest.reportFile))

    def test_canWriteCSV(self):
        sheet = Spreadsheet(self.factory, forkId=1).export()
        sheet.csv(SpreadsheetTest.testFile)
        self.assertTrue(os.path.isfile(SpreadsheetTest.testFile))

        with open(SpreadsheetTest.testFile, "r") as handle:
            line = handle.readline()
            self.assertEqual("path,MYSQL_8.0,MYSQL_5.5", line.strip())

    def _execute(self, dbName, query, params=None):
        try:
            conn = psycopg2.connect("dbname=%s" % self.dbName)
            cur = conn.cursor()
            cur.execute(query, params)
            conn.commit()
            return cur.fetchone()

        finally:
            if cur:
                cur.close()
            if conn:
                conn.close()

    def tearDown(self):
        self.db.drop()
        if os.path.isfile(SpreadsheetTest.testFile):
            os.remove(SpreadsheetTest.testFile)
        if os.path.isfile(SpreadsheetTest.reportFile):
            os.remove(SpreadsheetTest.reportFile)


if __name__ == '__main__':
    unittest.main()
