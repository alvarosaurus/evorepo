##
# python3 -m unittest test.MetricsTest.MetricsTest
# or
# python test/MetricsTest.py

import unittest
from evorepo.Repository import Repository
from evorepo.Postgres import PgDatabase, PgDAOFactory
from evorepo.Metrics import Metrics
from evorepo.Model import Project, Fork, Branch, File, Version, Contributor, Contribution
from MockGitConnector import MockGitConnector


class MetricsTest(unittest.TestCase):
    """
    Run this test on the test tree in testdata.
    """
    dbName = "test"
    projectName = "test"
    branchName = "test"
    startTag = "start"
    endTag = "end"
    rootPath = "test/testdata/testtree"

    def setUp(self):
        self.db = PgDatabase(self.dbName).create()
        self.factory = PgDAOFactory(self.dbName)
        fork = Fork(description="test fork")
        fork = self.factory.getForkDAO(fork).save()
        project = Project(name=self.projectName, forkId=fork.id)
        project = self.factory.getProjectDAO(project).save()
        branch = Branch(name=self.branchName, projectId=project.id)
        self.branch = self.factory.getBranchDAO(branch).save()
        repo = Repository(project, branch, self.rootPath)
        self.gitConnector = MockGitConnector()
        repo.list()
        for file in repo.fileList:
            file = self.factory.getFileDAO(file).save()
            version = Version(fileId=file.id, branchId=branch.id)
            self.factory.getVersionDAO(version).save()

    def test_checksum(self):
        metrics = Metrics(self.projectName, self.startTag, self.endTag, self.rootPath, self.dbName)
        metrics.addChecksums()
        fileList = self.factory.getFileDAO().list()
        versionList = []
        for file in fileList:
            version = self.factory.getVersionDAO().listVersionsByPath(file.id)[0]
            versionList.append(version)

        for version in versionList:
            self.assertTrue(version.checksum is not None)

    def test_size(self):
        metrics = Metrics(self.projectName, self.startTag, self.endTag, self.rootPath, self.dbName)
        metrics.addSizes()
        fileList = self.factory.getFileDAO().list()
        for file in fileList:
            version = self.factory.getVersionDAO().listVersionsByPath(file.id)[0]
            self.assertTrue(version.size is not None)
            if (file.path == "test1.txt"):
                self.assertEqual(1, version.size)
            if (file.path == "test2.txt"):
                self.assertEqual(2, version.size)
            if (file.path == "/sub1/test1.1.txt"):
                self.assertEqual(3, version.size)

    def test_parseAuthorLog(self):
        nameDict = self.gitConnector.getAuthorLog("test", self.startTag, self.endTag)
        self.assertEquals(29, len(nameDict))
        # MockGitConnector loads commits from the mock file gittest.log
        self.assertEquals(3, nameDict[Contributor.getNameChecksum("Testauthor030 Test")])

    def test_parseCommitterLog(self):
        nameDict = self.gitConnector.getCommitterLog("test", self.startTag, self.endTag)
        self.assertEquals(29, len(nameDict))
        # MockGitConnector loads commits from the mock file gittest.log
        self.assertEquals(3, nameDict[Contributor.getNameChecksum("Testauthor030 Test")])

    def test_edits(self):
        metrics = Metrics(
                self.projectName,
                self.startTag, self.endTag, self.rootPath, self.dbName, self.gitConnector)
        metrics.addEdits()

        # MockGitConnector loads commits from the mock file gittest.log
        # all files have the same commits in the mock file

        # there are 29 authors with a name
        self.assertEquals(29, len(self.factory.getContributorDAO().list()))
        # there are 29 edit objects (one for each author in a branch)
        self.assertEquals(29, len(self.factory.getContributionDAO().list(type=Contribution.EDIT)))

        # test author has 3 edits
        ctbr = self.factory.getContributorDAO().list(
                Contributor.getNameChecksum("Testauthor030 Test"))[0]

        edits = self.factory.getContributionDAO().list(
                requestedBranchId=metrics.branch.id,
                requestedContributorId=ctbr.id,
                type=Contribution.EDIT)
        self.assertEquals(3, edits[0].total)

    def test_commits(self):
        metrics = Metrics(self.projectName, self.startTag, self.endTag, self.rootPath, self.dbName, self.gitConnector)
        metrics.addCommits()

        # MockGitConnector loads commits from the mock file gittest.log
        # all files have the same commits in the mock file

        # there are 29 authors with a name
        self.assertEquals(29, len(self.factory.getContributorDAO().list()))
        # there are 29 edit objects (one for each author in a branch)
        self.assertEquals(29, len(self.factory.getContributionDAO().list(type=Contribution.COMMIT)))
        # test author has 3 edits
        ctbr = self.factory.getContributorDAO().list(
                Contributor.getNameChecksum("Testauthor030 Test"))[0]
        commits = self.factory.getContributionDAO().list(
                requestedBranchId=metrics.branch.id,
                requestedContributorId=ctbr.id,
                type=Contribution.COMMIT)
        self.assertEquals(3, commits[0].total)

#       def test_realGitConnector(self):
#           realGitConnector = GitConnector()           
#           nameDict = realGitConnector.getAuthorLog(repositoryPath=".")
#           self.assertEquals(2, len(nameDict))
#           self.assertEquals(10, nameDict["alvaro"])
#           self.assertEquals(5, nameDict["Alvaro"])
#           nameDict = realGitConnector.getCommitterLog(repositoryPath=".")
#           self.assertEquals(2, len(nameDict))
#           self.assertEquals(10, nameDict["alvaro"])
#           self.assertEquals(5, nameDict["Alvaro"])

    def tearDown(self):
        self.db.drop()
        #metrics = Metrics(self.projectName, self.startTag, self.endTag, self.rootPath, self.dbName, self.gitConnector)
        #metrics.addEdits()


if __name__ == '__main__':
    unittest.main()
