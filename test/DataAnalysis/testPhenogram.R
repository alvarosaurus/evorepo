library(testthat)
source("src/DataAnalysis/evorepo/SpreadSheet.R")
source("src/DataAnalysis/evorepo/EuclideanDistanceMatrix.R")
source("src/DataAnalysis/evorepo/ClusterTree.R")
source("src/DataAnalysis/evorepo/Phenogram.R")

print("Testing Phenogram")

# The file to save the diagram
destFile = "test/test.png"

cleanup = function() {
  # Remove the png file
  if ( file.exists(destFile ) ) {
    file.remove( destFile )
  }
}

#cleanup()

# Estimate a phylogenetic tree from test data
path = "test/DataAnalysis/testdata/MOCK_DATA.csv"
source <- Spreadsheet$new( path )
source$read()
distance <- EuclideanDistanceMatrix$new()
distance$compute( source )
cluster <- ClusterTree$new()
cluster$estimate( distance, "branch.1" )

# Test that a phylogenetic tree can be estimated from a distance matrix 
test_that("Test estimate tree",{
    diagram <- Phenogram$new( destFile )
    diagram$plot( cluster )
    # check taht a file was created
    expect_true( file.exists(destFile ) )
})



cleanup()

