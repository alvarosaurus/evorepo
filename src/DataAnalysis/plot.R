## Plot the phylogenetic tree
source("src/DataAnalysis/evorepo/Phenogram.R")
source("src/DataAnalysis/evorepo/NJTree.R")

## get the command-line arguments
args = commandArgs(trailingOnly=TRUE)

if (length(args)!=2) {
  stop("Input and output files must be supplied.n", call.=FALSE)
}

## path to file with tree data
sourcePath = args[1]
## The file to save the diagram
destFile = args[2]

## load the pre-calculated tree
tree <- NJTree$new()
tree$load(sourcePath)

## save the plot
diagram <- Phenogram$new(destFile)
diagram$plot(tree, use.edge.length = FALSE)

