library(R6)
library(ape)
source("src/DataAnalysis/evorepo/ATree.R")

#'
#' An implementation of a Tree, that uses Neighbour-Joining as clustering method for evaluating the tree.
#' 
NJTree <- R6Class( "NJTree",
  inherit = ATree,
  
  public = list(
    # public fields and methods
    
    estimate = function( distance, root=NULL, filter=NULL ) {
      "Estimate the tree from the distance matrix.
      @param distance ADistanceMatrix containing the dissimilarities between OTUs
      @param root name or index of the branch that roots the tree
      @return TRUE if the tree could be estimated from the distance matrix, FALSE otherwise
      "

      ## cluster using NJ
      ## hc <- nj( distance$matrix )
      ## self$phylo <- as.phylo(hc)
      if( is.null(root) ) {
          self$phylo <- as.phylo( nj( distance$matrix ))
      } else {
          self$phylo <- root(as.phylo( nj( distance$matrix )), root)
      }
      
      if( !is.null(filter) ) {
          self$phylo <- drop.tip( self$phylo, filter )
      }
      
      # return value
      return( !is.null( self$phylo ) )
    }
  )
)
