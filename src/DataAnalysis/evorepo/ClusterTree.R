library(R6)
library(ape)
source("src/DataAnalysis/evorepo/ATree.R")

#'
#' An implementation of a Tree, that uses UPGMA as clustering method for evaluating the tree.
#' 
#' @field method string Method to apply for clustering.
ClusterTree <- R6Class( "ClusterTree",
  inherit = ATree,
  
  public = list(
    # public fields and methods
    
    method = NULL,
    
    initialize = function( method = "UPGMA") {
      "Initialize the Phylogeny with a clustering method
      @param method string Method to apply for clustering.
      "
      self$method = method
    },
  
    estimate = function( distance, root ) {
      "Estimate the tree from the distance matrix.
      @param distance ADistanceMatrix containing the dissimilarities between OTUs
      @param root name or index of the branch that roots the tree
      @return TRUE if the tree could be estimated from the distance matrix, FALSE otherwise
      "

      # cluster using UPGMA
      #hc <- hclust( distance$matrix, "average" )
      self$phylo <- root(as.phylo( hclust( distance$matrix, "average" )), root)
      #self$phylo <- drop.tip( self$phylo, root )
      #self$phylo <- as.phylo(hc)
      
      # return value
      return( !is.null( self$phylo ) )
    }
  )
)
