##
# usage:
#
# python3 -m export <dbName> <fileName>
#
import argparse
import traceback
from evorepo.Postgres import PgDAOFactory
from evorepo.Spreadsheet import Spreadsheet

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(
            description='Export a database into a spreadsheet suitable for taxonomic analysis.')
        parser.add_argument("dbName", help="")
        parser.add_argument("forkDescription", help="")
        parser.add_argument("fileName", help="")
        parser.add_argument("exportType", help="")
        args = parser.parse_args()

        factory = PgDAOFactory(args.dbName)
        fork = factory.getForkDAO().find(args.forkDescription)
        sheet = Spreadsheet(factory, fork.id)
        exportType = int(args.exportType)
        sheet.export(type=exportType)
        sheet.csv(args.fileName)

    except Exception:
        print(traceback.format_exc())
