##
# usage:
#
# export PYTHONPATH=${PYTHONPATH}:src/
# python3 -m collect <repoName> <branchName> <path> <dbName>
#
import argparse
import traceback
from evorepo.Postgres import PgDAOFactory
from evorepo.Model import Fork, Project, Branch, Version
from evorepo.Repository import Repository


class Collection(object):
    factory = None

    def __init__(self, dbName):
        self.dbName = dbName
        self.factory = PgDAOFactory(self.dbName)

    def createFork(self, description):
        fork = Fork(description=description)
        return self.factory.getForkDAO(fork).save()

    def createProject(self, name, fork):
        project = Project(name=name, forkId=fork.id)
        return self.factory.getProjectDAO(project).save()

    def createBranch(self, name, project):
        branch = Branch(name=name, projectId=project.id)
        return self.factory.getBranchDAO(branch).save()

    def createRepository(self, project, branch, rootPath):
        repo = Repository(project, branch, rootPath)
        repo.list()
        return repo

    def saveRepo(self, repo):
        fileDAO = self.factory.getFileDAO()
        versionDAO = self.factory.getVersionDAO()

        for file in repo.fileList:
            fileDAO.file = file
            file = fileDAO.save()
            versionDAO.version = Version(
                fileId=file.id, branchId=repo.branch.id)
            versionDAO.save()


if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(
            description='Collect data from a repository into a database')
        parser.add_argument("forkName", help="")
        parser.add_argument("repoName", help="")
        parser.add_argument("branchName", help="")
        parser.add_argument("path", help="")
        parser.add_argument("dbName", help="")
        args = parser.parse_args()

        collection = Collection(args.dbName)
        fork = collection.createFork(args.forkName)
        project = collection.createProject(args.repoName, fork)
        branch = collection.createBranch(args.branchName, project)
        repo = collection.createRepository(project, branch, args.path)
        collection.saveRepo(repo)

    except Exception:
        print(traceback.format_exc())
