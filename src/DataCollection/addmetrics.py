##
# usage:
#
# python3 -m src.addmetrics <repoName> <branchName> <path> <dbName>
#
import argparse
import traceback
from evorepo.Metrics import Metrics
from evorepo.GitConnector import GitConnector

if __name__ == '__main__':
    try:
        parser = argparse.ArgumentParser(
                description='Collect metrics for each file version into a database')
        parser.add_argument("repoName", help="")
        parser.add_argument("startTag", help="")
        parser.add_argument("endTag", help="")
        parser.add_argument("path", help="")
        parser.add_argument("dbName", help="")
        args = parser.parse_args()

        metrics = Metrics(args.repoName, args.startTag, args.endTag, args.path, args.dbName, GitConnector())
        metrics.addChecksums()
        metrics.addSizes()
        metrics.addEdits()
        metrics.addCommits()

    except Exception:
        print(traceback.format_exc())
