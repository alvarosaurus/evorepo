import subprocess
from evorepo.Model import Contributor


class AGitConnector(object):
    def __init__(self):
        """
        Always override the constructor, even if the constructor has no arguments.
        """
        raise NotImplementedError()

    def getAuthorLog(self, repositoryPath):
        raise NotImplementedError()

    @staticmethod
    def parseContributorLog(log):
        # get the names of all the contributors
        lines = log.split('\n')
        nameList = []
        for name in lines:
            # if the name is unknown, skip
            if len(name) == 0 or name == 'unknown':
                continue
            nameList.append(Contributor.getNameChecksum(name))

        # count commits per author, make it a dict
        nameDict = {}
        counts = [{x: nameList.count(x)} for x in set(nameList)]
        for c in counts:
            nameDict.update(c)
        return nameDict


class GitConnector(AGitConnector):
    def __init__(self):
        pass

    def getAuthorLog(self, repositoryPath, startTag, endTag):
        proc = subprocess.Popen(
                ['/usr/bin/git', 'log', '--pretty=format:%an', "%s..%s" % (startTag, endTag)],
                cwd=repositoryPath,
                stderr=subprocess.STDOUT,
                stdout=subprocess.PIPE)
        data = proc.communicate()
        nameDict = AGitConnector.parseContributorLog(data[0].decode('latin-1'))
        return nameDict

    def getCommitterLog(self, repositoryPath, startTag, endTag):
        proc = subprocess.Popen(
                ['/usr/bin/git', 'log', '--pretty=format:%cn', "%s..%s" % (startTag, endTag)],
                cwd=repositoryPath,
                stderr=subprocess.STDOUT,
                stdout=subprocess.PIPE)
        data = proc.communicate()
        nameDict = AGitConnector.parseContributorLog(data[0].decode('latin-1'))
        return nameDict
