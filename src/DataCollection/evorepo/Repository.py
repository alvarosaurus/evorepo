import os
import re
from evorepo.Model import File as FileModel


class Repository(object):
    # Class variable fileList contains a list of all files except hidden files
    fileList = None

    def __init__(self, project, branch, rootPath):
        self.project = project
        self.branch = branch
        self.rootPath = rootPath

    def list(self):
        """ Walk through the folder on the file system and return the relative paths of the files.

        @return list of File models
        """
        if self.fileList is not None:
            return self.fileList

        self.fileList = []
        count = 0
        for root, dirs, files in os.walk(self.rootPath):
            shortPath = root[len(self.rootPath):]
            # print( "Processing %d files" % len(files) )
            for name in files:
                count += 1
                if count % 1000 == 0:
                    print("%d files processed" % count)
                # skip hidden files
                if re.match("^\.", name):
                    continue
                # skip .git files
                if re.search("\.git", root):
                    continue

                # add the file to the list
                fullPath = os.path.join(shortPath, name)
                # add a slash when the root path is a subdir of the git repo
                # if not re.match( "^/", fullPath ): fullPath = "/%s" % fullPath

                file = FileModel(path=fullPath)
                self.fileList.append(file)
                        
        return self.fileList
