import os
from evorepo.Postgres import PgDAOFactory
from evorepo.Model import Project, Branch, Version, Contributor, Contribution
from evorepo.Repository import Repository


class Metrics():
    def __init__(self, projectName, startTag, endTag, path, dbName, gitConnector=None):
        self.path = path
        self.startTag = startTag
        self.endTag = endTag
        self.gitConnector = gitConnector
        self.factory = PgDAOFactory(dbName)
        self.project = Project(name=projectName)
        self.project = self.factory.getProjectDAO(self.project).save()
        self.branch = Branch(name=endTag, projectId=self.project.id)
        self.branch = self.factory.getBranchDAO(self.branch).save()
        self.repository = Repository(self.project, self.branch, self.path)
        self.repository.list()

    def addSizes(self):
        fileDAO = self.factory.getFileDAO()
        versionDAO = self.factory.getVersionDAO()
        count = 0
        skipped = 0
        for file in self.repository.fileList:
            count += 1
            if count % 1000 == 0:
                    print("addSizes: %d files processed" % count)
            # get the file object for the path
            file = fileDAO.list(file.path)[0]
            # get the version of this file for this branch
            list = versionDAO.listVersionsByPath(
                    requestedFileId=file.id, requestedBranchId=self.branch.id)
            if len(list) == 0:
                # print("skipped %s" % file.path)
                skipped += 1
                continue
            version = list[0]
            # get the absolute paths
            path = self.getRealPath(file)
            # count the number of lines in the file
            version.size = Version.getSize(path)
            versionDAO.version = version
            versionDAO.save()
        print("Add sizes skipped %d / %d files" % (skipped, count))

    def addChecksums(self):
        fileDAO = self.factory.getFileDAO()
        versionDAO = self.factory.getVersionDAO()

        count = 0
        skipped = 0
        # iterate the file paths on the file system
        for file in self.repository.fileList:
            count += 1
            if count % 1000 == 0:
                print("addChecksums: %d files processed" % count)
            # get the file object for the path
            # print(file.path)
            file = fileDAO.list(file.path)[0]
            # get the version of this file for this branch
            list = versionDAO.listVersionsByPath(
                    requestedFileId=file.id,
                    requestedBranchId=self.branch.id)
            if len(list) == 0:
                # print("skipped %s" % file.path)
                skipped += 1
                continue
            version = list[0]
            # get the absolute paths
            path = self.getRealPath(file)
            # calculate the checksum of the version of the file
            version.checksum = Version.getChecksum(path)
            versionDAO.version = version
            versionDAO.save()
        print("Add checksum skipped %d / %d files" % (skipped, count))

    def getRealPath(self, file):
        # join doesn't work if a path starts with a slash
        filePath = file.path
        if filePath[0] == '/':
            filePath = filePath[1:]
        return os.path.join(self.path, filePath)

    def addEdits(self):
        # get the DAOs outside the loop
        contributorDAO = self.factory.getContributorDAO()
        contributionDAO = self.factory.getContributionDAO()

        # iterate the file paths on the file system
        count = 0
        nameDict = self.gitConnector.getAuthorLog(
                repositoryPath=self.path,
                startTag=self.startTag,
                endTag=self.endTag)

        for name, count in nameDict.items():
            # save the contributor
            contributor = Contributor(nickname=name)
            contributorDAO.contributor = contributor
            contributor = contributorDAO.save()
            # save the edit
            editList = contributionDAO.list(
                    requestedContributorId=contributor.id,
                    requestedBranchId=self.branch.id,
                    type=Contribution.EDIT)
            if editList is not None and len(editList) != 0:
                edit = editList[0]
                edit.total += count
                contributionDAO.contribution = edit
                contributionDAO.update()
            else:
                edit = Contribution(
                    contributorId=contributor.id,
                    branchId=self.branch.id,
                    total=count,
                    type=Contribution.EDIT)
                contributionDAO.contribution = edit
                contributionDAO.save()

    def addCommits(self):
        # get the DAOs outside the loop
        contributorDAO = self.factory.getContributorDAO()
        contributionDAO = self.factory.getContributionDAO()

        # iterate the file paths on the file system
        count = 0
        nameDict = self.gitConnector.getCommitterLog(
                repositoryPath=self.path,
                startTag=self.startTag,
                endTag=self.endTag)
        for name, count in nameDict.items():
            # save the contributor
            contributor = Contributor(nickname=name)
            contributorDAO.contributor = contributor
            contributor = contributorDAO.save()

            # save the edit
            commitList = contributionDAO.list(
                    requestedContributorId=contributor.id,
                    requestedBranchId=self.branch.id,
                    type=Contribution.COMMIT)
            if commitList is not None and len(commitList) != 0:
                commit = commitList[0]
                commit.total += count
                contributionDAO.contribution = commit
                contributionDAO.update()
            else:
                commit = Contribution(
                        contributorId=contributor.id,
                        branchId=self.branch.id,
                        total=count,
                        type=Contribution.COMMIT)
                contributionDAO.contribution = commit
                contributionDAO.save()
