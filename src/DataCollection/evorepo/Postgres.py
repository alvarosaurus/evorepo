import subprocess
import os
import psycopg2
import traceback
from evorepo.Database import AbstractDatabase, AbstractDAOFactory, AbstractDAO
from evorepo.Model import Fork, Project, Branch, File, Version, Contribution, Contributor


class PgDatabase(AbstractDatabase):

    DB_CREATE_FILE = os.path.join(os.getcwd(), "install/createTables.sql")

    def __init__(self, dbName):
        self.dbName = dbName

    def create(self):
        subprocess.check_call(["/usr/bin/createdb", self.dbName])
        subprocess.check_call(
            ["/usr/bin/psql", "--quiet", self.dbName, "-f", PgDatabase.DB_CREATE_FILE])
        return self

    def load(self, filePath):
        subprocess.check_call(["/usr/bin/psql", "--quiet", self.dbName, "-f", filePath])

    def drop(self):
        subprocess.check_call(["/usr/bin/dropdb", self.dbName])


class PgDAOFactory(AbstractDAOFactory):
    def __init__(self, dbName):
        self.dbName = dbName

    def getForkDAO(self, fork=None):
        return ForkDAO(self.dbName, fork)

    def getProjectDAO(self, project=None):
        return ProjectDAO(self.dbName, project)

    def getBranchDAO(self, branch=None):
        return BranchDAO(self.dbName, branch)

    def getFileDAO(self, file=None):
        return FileDAO(self.dbName, file)

    def getVersionDAO(self, version=None):
        return VersionDAO(self.dbName, version)

    def getContributorDAO(self, contributor=None):
        return ContributorDAO(self.dbName, contributor)

    def getContributionDAO(self, contribution=None):
        return ContributionDAO(self.dbName, contribution)


class BasePgDAO(AbstractDAO):

    def __init__(self, dbName):
        self.dbName = dbName
        self.conn = psycopg2.connect("dbname=%s" % self.dbName)

    def _execute(self, query, params=None):
        cur = self.conn.cursor()
        try:
            cur.execute(query, params)
            self.conn.commit()
            return cur.fetchone()

        finally:
            cur.close()

    def _list(self, query, params=None):
        cur = self.conn.cursor()
        try:
            cur.execute(query, params)
            self.conn.commit()
            return cur.fetchall()

        finally:
            cur.close()

    def listProjectsByFork(self, requestedForkId):
        response = []
        projects = self._list(
            """select id from project where forkId = %(forkId)s""",
            {'forkId': requestedForkId})
        if len(projects) == 0:
            return []
        if len(projects) > 2:
            raise Exception("Only 2 projects per fork allowed")
        for project in projects:
            response.append(project[0])
        return response


class ForkDAO(BasePgDAO):
    """
    Represents fork of at least 2 projects
    """
    def __init__(self, dbName, fork):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.fork = fork

    def save(self):
        # try to get the project by name
        response = self._execute(
            """SELECT id from Fork where description=%(description)s""",
            {'description': self.fork.description})

        # If fork by this description doesn't exist, create one
        if response is None:
            response = self._execute(
                """INSERT INTO Fork (description) VALUES( %(description)s ) returning id;""",
                {'description': self.fork.description})

        self.fork.id = response[0]
        return self.fork

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,description from Fork where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read Fork with id: %s" % requestedId)
        id = response[0]
        description = response[1]
        self.fork = Fork(id=id, description=description)
        return self.fork

    def find(self, requestedDescription=None):
        response = self._execute(
            """SELECT id,description from Fork where description=%(description)s""",
            {'description': requestedDescription})

        if response is None:
            raise Exception("Could not read Fork with description: %s" % requestedDescription)
        id = response[0]
        description = response[1]
        self.fork = Fork(id=id, description=description)
        return self.fork


class ProjectDAO(BasePgDAO):
    """
    Represents a forked project
    """
    def __init__(self, dbName, project):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.project = project
        
    def save(self):
        # try to get the project by name
        response = self._execute(
            """SELECT id from Project where name=%(name)s""",
            {'name': self.project.name})

        # If project by this name doesn't exist, create one
        if response is None:
            response = self._execute(
                """INSERT INTO Project (name, forkId) VALUES( %(name)s, %(forkId)s ) returning id;""",
                {'name': self.project.name, 'forkId': self.project.forkId})

        self.project.id = response[0]
        return self.project

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,name,forkId from Project where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read Project with id: %s" % requestedId)
        id = response[0]
        name = response[1]
        forkId = response[2]
        self.project = Project(id=id, name=name, forkId=forkId)
        return self.project
        
    def readByName(self, requestedName):
        response = self._execute(
            """SELECT id,name,forkId from Project where name=%(name)s""",
            {'name': requestedName})

        if response is None:
            raise Exception("Could not read Project with name: %s" % requestedName)
        id = response[0]
        name = response[1]
        forkId = response[2]
        self.project = Project(id=id, name=name, forkId=forkId)
        return self.project

    def list(self, requestedForkId=None):
        projectList = []
        if requestedForkId is not None:
            response = self._list(
                """SELECT id,name,forkId from Project where forkId=%(forkId)s""",
                {'forkId': requestedForkId})
        else:
            response = self._list("""SELECT id,name,forkId from Project""")

        for r in response:
            id = r[0]
            name = r[1]
            forkId = r[2]
            projectList.append(Project( id=id, name=name, forkId=forkId))

        return projectList


class BranchDAO(BasePgDAO):
    """
    Represents a branch of a project
    """
    def __init__(self, dbName, branch):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.branch = branch

    def save(self):
        print("save branch")
        # try to get the branch by name
        response = self._execute(
            """SELECT id from Branch where name=%(name)s and projectId=%(projectId)s""",
            {'name': self.branch.name, 'projectId': self.branch.projectId})

        # If branch by this name doesn't exist in this project, create one
        if response is None:
            response = self._execute(
                """INSERT INTO Branch (name, projectId) VALUES( %(name)s, %(projectId)s ) returning id;""",
                {'name': self.branch.name, 'projectId': self.branch.projectId})

        self.branch.id = response[0]
        return self.branch

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,name,projectId from Branch where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read Branch with id: %s" % requestedId)

        id = response[0]
        name = response[1]
        projectId = response[2]
        self.branch = Branch(id=id, projectId=projectId, name=name)
        return self.branch

    def list(self, requestedProjectId=None):
        branchList = []
        if requestedProjectId is not None:
            response = self._list(
                """SELECT id,name,projectId from Branch where projectId=%(projectId)s""",
                {'projectId': requestedProjectId})
        else:
            response = self._list("""SELECT id,name,projectId from Branch""")

        for r in response:
            id = r[0]
            name = r[1]
            projectId = r[2]
            branchList.append(Branch(id=id, name=name, projectId=projectId))

        return branchList

    def listByFork(self, requestedForkId):
        branchList = []
        projects = self.listProjectsByFork(requestedForkId)

        if len(projects) == 2:
            response = self._list(
                """SELECT id,name,projectId from Branch
                where projectId = %(project1)s or projectId = %(project2)s""",
                {
                    'project1': projects[0],
                    'project2': projects[1]
                })
        elif len(projects) == 1:
            response = self._list(
                """SELECT id,name,projectId from Branch
                where projectId = %(project1)s""",
                {'project1': projects[0]})

        for r in response:
            id = r[0]
            name = r[1]
            projectId = r[2]
            branchList.append(Branch(id=id, name=name, projectId=projectId))

        return branchList


class FileDAO(BasePgDAO):
    """
    Represents a file spanning any branches, identified by its path
    """
    def __init__(self, dbName, file):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.file = file

    def save(self):
        # try to get the file by path
        response = self._execute(
            """SELECT id from File where path=%(path)s""",
            {'path': self.file.path})

        # If file with this path doesn't exist in this branch, create one
        if response is None:
            response = self._execute(
                """INSERT INTO File (path) VALUES( %(path)s ) returning id;""",
                {'path': self.file.path})

        self.file.id = response[0]
        return self.file

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,path from File where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read File with id: %s" % requestedId)

        id = response[0]
        pathStr = response[1]
        self.file = File(id=id, path=pathStr)

        return self.file

    def list(self, requestedPath=None):
        reponse = []
        if requestedPath is None:
            results = self._list("SELECT id,path from File order by path")
        else:
            results = self._list(
                "SELECT id,path from File where path=%(path)s",
                {'path': requestedPath})

        for result in results:
            id = result[0]
            pathStr = result[1]
            reponse.append(File(id=id, path=pathStr))
        return reponse

    def listByFork(self, requestedForkId):
        reponse = []
        projects = self.listProjectsByFork(requestedForkId)

        if len(projects) == 2:
            results = self._list(
                """select file.id,file.path from file,version,branch where file.id=version.fileid 
                and version.branchId = branch.id
                and ( branch.projectid = %(project1)s or branch.projectid = %(project2)s )
                group by file.id;""",
                {
                    'project1': projects[0],
                    'project2': projects[1]
                })
        elif len(projects) == 1:
            results = self._list(
                """select file.id,file.path from file,version,branch where file.id=version.fileid 
                and version.branchId = branch.id
                and branch.projectid = %(project1)s
                group by file.id;""",
                {'project1': projects[0]})

        for result in results:
            id = result[0]
            pathStr = result[1]
            reponse.append(File(id=id, path=pathStr))

        return reponse


class VersionDAO(BasePgDAO):
    """
    Represent a version of a file in a branch.
    """
    def __init__(self, dbName, version):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.version = version

    def save(self):
        # try to read version by id, if exists update
        exists = False
        if self.version.id is not None:
            exists = self._execute(
                """SELECT id from Version where id=%(id)s""",
                {'id': self.version.id}) is not None

        if exists:
            response = self._execute(
                """UPDATE Version SET
                fileId = %(fileId)s, branchId = %(branchId)s, checksum = %(checksum)s , size = %(size)s
                where id=%(id)s returning id;""",
                {
                    'id': self.version.id,
                    'fileId': self.version.fileId,
                    'branchId': self.version.branchId,
                    'checksum': self.version.checksum,
                    'size': self.version.size
                })

        else:
            response = self._execute(
                """INSERT INTO Version (fileId, branchId, checksum, size) 
                VALUES( %(fileId)s, %(branchId)s, %(checksum)s, %(size)s ) returning id;""", 
                {
                    'fileId': self.version.fileId,
                    'branchId': self.version.branchId,
                    'checksum': self.version.checksum,
                    'size': self.version.size
                })
            id = response[0]
            self.version = self.read(id)
            return self.version

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,fileId,branchId,checksum from Version where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read Version with id: %s" % requestedId)

        id = response[0]
        fileId = response[1]
        branchId = response[2]
        checksum = response[3]
        self.version = Version(id=id, fileId=fileId, branchId=branchId, checksum=checksum)

        return self.version

    def listVersionsByPath(self, requestedFileId, requestedBranchId=None):
        versionList = []
        if requestedBranchId is None:
            response = self._list(
                """SELECT id,fileId,branchId,checksum,size from Version
                where fileId=%(fileId)s""",
                {'fileId': requestedFileId})
        else:
            response = self._list(
                """SELECT id,fileId,branchId,checksum,size from Version
                where fileId=%(fileId)s and branchId=%(branchId)s""",
                {'fileId': requestedFileId, 'branchId': requestedBranchId})

        for r in response:
            id = r[0]
            fileId = r[1]
            branchId = r[2]
            checksum = r[3]
            size = r[4]
            versionList.append(Version(id=id, fileId=fileId, branchId=branchId, checksum=checksum, size=size))

        return versionList

    def listBranchesByPath(self, requestedFileId):
        result = []
        present = self._list(
            """select branchId from Version where fileId=%(fileId)s order by branchId""",
            {'fileId': requestedFileId})
        for p in present:
            result.append(p[0])
        return result


class ContributorDAO(BasePgDAO):
    """
    Represent a person working within the project.

    Persons contribute edits or commits.
    Identified by nickname.
    """
    def __init__(self, dbName, contributor):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.contributor = contributor

    def save(self):
        # try to get the contributor by nickname
        try:
            nickname = self.contributor.nickname
            response = self._execute(
                """SELECT id from Contributor where nickname=%(nickname)s""",
                {'nickname': nickname})

            # If author with this nickname doesn't exist, create one
            if response is None:
                response = self._execute(
                    """INSERT INTO Contributor (nickname)
                    VALUES( %(nickname)s ) returning id;""",
                    {'nickname': nickname})

            self.contributor.id = response[0]
            self.contributor.nickname = nickname

        except Exception as e:
            print(e)
            print(self.contributor.nickname)
            traceback.print_exc()

        return self.contributor

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,nickname from Contributor where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read Contributor with id: %s" % requestedId)

        id = response[0]
        nickname = response[1]
        self.contributor = Contributor(id=id, nickname=nickname)

        return self.contributor

    def list(self, requestedNickname=None):
        contributorList = []
        if requestedNickname is not None:
            response = self._list(
                """SELECT id,nickname from Contributor where nickname=%(nickname)s""",
                {'nickname': requestedNickname})
        else:
            response = self._list("""SELECT id,nickname from Contributor""")

        for r in response:
            id = r[0]
            nickname = r[1]
            contributorList.append(Contributor(id=id, nickname=nickname))

        return contributorList

    def listByFork(self, requestedForkId):
        ctbrIds = []
        projects = self.listProjectsByFork(requestedForkId)

        if len(projects) == 2:
            results = self._list(
                """select distinct contributor.id from contributor,contribution,branch 
                where contributor.id=contribution.contributorid and contribution.branchId = branch.id 
                and ( branch.projectid = %(project1)s or branch.projectid = %(project2)s )""",
                {
                    'project1': projects[0],
                    'project2': projects[1]
                })
        elif len(projects) == 1:
            results = self._list(
                """select distinct contributor.id from contributor,contribution,branch 
                where contributor.id=contribution.contributorid and contribution.branchId = branch.id 
                and branch.projectid = %(project1)s""",
                {'project1': projects[0]})

        for result in results:
            ctbrIds.append(result[0])

        ctbrIds = list(set(ctbrIds))
        response = []
        for id in ctbrIds:
            response.append(self.read(id))
                
        return response


class ContributionDAO(BasePgDAO):
    """
    Represents an edit or commit by a contributor to a file version.
    """
    def __init__(self, dbName, contribution):
        super(self.__class__, self).__init__(dbName)
        self.dbName = dbName
        self.contribution = contribution

    def save(self):
        response = self._execute(
            """INSERT INTO Contribution (contributorId, branchId, total, type) 
            VALUES( %(contributorId)s, %(branchId)s, %(total)s, %(type)s ) returning id;""", 
            {
                'contributorId': self.contribution.contributorId,
                'branchId': self.contribution.branchId,
                'total': self.contribution.total,
                'type': self.contribution.type
            })
        self.contribution.id = response[0]
        return self.contribution

    def update(self):
        response = self._execute(
            """UPDATE Contribution 
            set contributorId=%(contributorId)s, branchId=%(branchId)s, total=%(total)s, type=%(type)s 
            where id=%(id)s returning id;""",
            {
                'id': self.contribution.id,
                'contributorId': self.contribution.contributorId,
                'branchId': self.contribution.branchId,
                'total': self.contribution.total,
                'type': self.contribution.type
            })
        return self.contribution

    def read(self, requestedId):
        response = self._execute(
            """SELECT id,contributorId,branchId,total,type from Contribution where id=%(id)s""",
            {'id': requestedId})

        if response is None:
            raise Exception("Could not read Contribution with id: %s" % requestedId)

        id = response[0]
        contributorId = response[1]
        branchId = response[2]
        total = response[3]
        type = response[4]
        self.contribution = Contribution(id=id, contributorId=contributorId, branchId=branchId, total=total, type=type)

        return self.contribution

    def list(self, requestedBranchId=None, requestedContributorId=None, type=None):
        response = []
        if requestedBranchId is not None and requestedContributorId is not None and type is not None:
            results = self._list(
                """SELECT id,contributorId,branchId,total,type from Contribution 
                where contributorId=%(contributorId)s and branchId=%(branchId)s and type=%(type)s""",
                {
                    'contributorId': requestedContributorId,
                    'branchId': requestedBranchId,
                    'type': type
                })
        elif type is not None:
            results = self._list(
                "SELECT id,contributorId,branchId,total,type from Contribution where type=%(type)s",
                {'type': type})

        else:
            results = self._list("SELECT id,contributorId,branchId,total,type from Contribution")

        for result in results:
            id = result[0]
            contributorId = result[1]
            branchId = result[2]
            total = result[3]
            type = result[4]
            response.append(
                Contribution(
                    id=id, contributorId=contributorId, branchId=branchId, total=total, type=type))

        return response

    def listBranchesByContributor(self, requestedContributorId, requestedType=None):
        result = []
        if requestedType is not None:
            present = self._list(
                """select branchId from Contribution
                where contributorId=%(contributorId)s
                and type = %(type)s
                order by branchId""",
                {
                    'contributorId': requestedContributorId,
                    'type': requestedType
                })
        else:
            present = self._list(
                """select branchId from Contribution
                where contributorId=%(contributorId)s order by branchId""",
                {'contributorId': requestedContributorId})
        for p in present:
            result.append(p[0])
        return result
