import os
from evorepo.Model import Contribution


class Spreadsheet(object):
    ALL = 1
    CODE = 2
    TEAM = 3
    COMMITTERS = 4

    def __init__(self, factory, forkId):
        self.factory = factory
        self.forkId = forkId
        self.rows = []
        self.allBranches = self.factory.getBranchDAO().listByFork(forkId)
        self.report = {
            'file_present': 0, 'file_checksum': 0, 'file_size': 0,
            'file_present_effective': 0, 'file_checksum_effective': 0, 'file_size_effective': 0,
            'ctbr_present': 0, 'ctbr_edit': 0, 'ctbr_commit': 0,
            'ctbr_present_effective': 0, 'ctbr_edit_effective': 0, 'ctbr_commit_effective': 0
            }
        self.categories = self._makeCategoryNames()

    def export(self, type=ALL):
        # export code-related characteristics
        if (type == Spreadsheet.ALL or type == Spreadsheet.CODE):
            print("exporting code characteristics")
            fileDAO = self.factory.getFileDAO()
            files = fileDAO.listByFork(requestedForkId=self.forkId)
            total = len(files)
            versionDAO = self.factory.getVersionDAO()
            count = 0
            for file in files:
                count += 1
                self.appendRow(self.markPresent(file, versionDAO), 'file_present')
                self.appendRow(self.markChecksums(file, versionDAO), 'file_checksum')
                self.appendRow(self.markSizes(file, versionDAO), 'file_size')
                if count % 1000 == 0:
                    print("%d / %d paths processed" % (count, total))

        # export team-related characteristics
        if (type == Spreadsheet.ALL or type == Spreadsheet.TEAM):
            print("exporting team characteristics")
            ctbrDAO = self.factory.getContributorDAO()
            ctbrList = ctbrDAO.listByFork(requestedForkId=self.forkId)
            total = len(ctbrList)
            contributionDAO = self.factory.getContributionDAO()
            count = 0
            for ctbr in ctbrList:
                self.appendRow(self.markCommit(ctbr, contributionDAO), 'ctbr_commit')
                count += 1
                self.appendRow(self.markContributor(ctbr, contributionDAO), 'ctbr_present')
                self.appendRow(self.markEdit(ctbr, contributionDAO), 'ctbr_edit')
                if count % 100 == 0:
                        print("%d / %d contributors processed" % (count, total))

        # export committer characteristics only
        if (type == Spreadsheet.COMMITTERS):
            print("exporting committer characteristics")
            ctbrDAO = self.factory.getContributorDAO()
            ctbrList = ctbrDAO.listByFork(requestedForkId=self.forkId)
            total = len(ctbrList)
            contributionDAO = self.factory.getContributionDAO()
            count = 0
            for ctbr in ctbrList:
                count += 1
                self.appendRow(self.markCommit(ctbr, contributionDAO), 'ctbr_commit')
                if count % 100 == 0:
                    print("%d / %d contributors processed" % (count, total))

        return self

    def appendRow(self, newRow, reporttag):
        if newRow is None:
            return
        self.report[reporttag] += 1
        if (self.filter(newRow)):
            self.report['%s_effective' % reporttag] += 1
            self.rows.append(newRow)

    def filter(self, row):
        """
        Return TRUE if row is effective,
        FALSE is row can be discarded
        """
        na = False
        if any(val == 'NA' for val in row.branches.values()):
            na = True

        return not (na)

    def markContributor(self, ctbr, contributionDAO):
        newRow = Row(ctbr.nickname)
        present = contributionDAO.listBranchesByContributor(ctbr.id)
        for branch in self.allBranches:
            if branch.id in present:
                newRow.branches[branch.id] = 'TRUE'
            else:
                newRow.branches[branch.id] = 'FALSE'

        return newRow

    def markEdit(self, ctbr, contributionDAO):
        newRow = Row("Edits %s" % ctbr.nickname)
        present = contributionDAO.listBranchesByContributor(ctbr.id, requestedType=Contribution.EDIT)

        for branch in self.allBranches:
            if branch.id in present:
                total = contributionDAO.list(
                    requestedBranchId=branch.id,
                    requestedContributorId=ctbr.id,
                    type=Contribution.EDIT)[0].total
                newRow.branches[branch.id] = total
            else:
                newRow.branches[branch.id] = 0

        return newRow

    def markCommit(self, ctbr, contributionDAO):
        newRow = Row("Commits %s" % ctbr.nickname)
        present = contributionDAO.listBranchesByContributor(ctbr.id, requestedType=Contribution.COMMIT)

        for branch in self.allBranches:
            if branch.id in present:
                total = contributionDAO.list(
                    requestedBranchId=branch.id,
                    requestedContributorId=ctbr.id,
                    type=Contribution.COMMIT)[0].total
                newRow.branches[branch.id] = total
            else:
                newRow.branches[branch.id] = 0

        return newRow

    def markPresent(self, file, versionDAO):
        newRow = Row(file.path)
        present = versionDAO.listBranchesByPath(file.id)
        for branch in self.allBranches:
            if branch.id in present:
                newRow.branches[branch.id] = 'TRUE'
            else:
                newRow.branches[branch.id] = 'FALSE'

        return newRow

    def markChecksums(self, file, versionDAO):
        newRow = Row("Checksum %s" % file.path)
        checksums = []
        present = versionDAO.listBranchesByPath(file.id)
        for branch in self.allBranches:
            if branch.id in present:
                list = versionDAO.listVersionsByPath(file.id, branch.id)
                if len(list) != 0:
                    checksum = list[0].checksum
                    if checksum not in checksums:
                        checksums.append(checksum)
                    index = checksums.index(checksum)
                    newRow.branches[branch.id] = self.categories[index]
            else:
                return None

        return newRow

    def markSizes(self, file, versionDAO):
        newRow = Row("Size %s" % file.path)
        present = versionDAO.listBranchesByPath(file.id)
        for branch in self.allBranches:
            list = versionDAO.listVersionsByPath(file.id, branch.id)
            if branch.id in present:
                size = list[0].size
                if size is None:
                    return None
                else:
                    newRow.branches[branch.id] = list[0].size
            else:
                return None

        return newRow

    def csv(self, fileName):
        headersStr = self._makeHeaderString()

        with open(fileName, "w") as handle:
            handle.write(headersStr)
            handle.write('\n')
            for row in self.rows:
                handle.write(row.asString())
                handle.write('\n')

            self.saveReport(fileName)

    def saveReport(self, fileName):
        reportFileName = "%s_report.txt" % (os.path.splitext(fileName)[0])
        sortedKeys = self.report.keys()
        sortedKeys = sorted(sortedKeys)
        with open(reportFileName, "w") as handle:
            for key in sortedKeys:
                handle.write("%s: %d" % (key, self.report[key]))
                handle.write('\n')

    def _makeCategoryNames(self):
        # categories = [ 'AA', 'AB'...'ZZ']
        categories = []
        for prefix in [chr(i) for i in range(ord('A'),ord('Z')+1)]:
            cname = ["%s%s" % (prefix, chr(i)) for i in range(ord('A'), ord('Z')+1)]
            if cname != 'NA':
                categories += cname

        return categories

    def _makeHeaderString(self):
        headers = ["path"]
        for branch in self.allBranches:
            currentBranch = self.factory.getBranchDAO().read(branch.id)
            headers.append(currentBranch.name)

        headersStr = ""
        for h in headers:
            headersStr += "%s," % h
        headersStr = headersStr[:len(headersStr)-1]
        return headersStr

    def asString(self):
        response = ""
        for row in self.rows:
            response += ("%s\n" % row.asString())
        return response


class Row(object):
    def __init__(self, name):
        self.name = name
        self.branches = dict()

    def asString(self):
        response = "%s," % self.name
        for key in self.branches:
            response += "%s," % self.branches[key]

        response = response[:len(response)-1]
        return response
