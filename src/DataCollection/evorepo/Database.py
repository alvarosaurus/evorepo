class AbstractDatabase(object):

	def __init__(self):
		"""
		Always override the constructor, even if the constructor has no arguments.
		"""
		raise NotImplementedError()

		
	def create(self):
		raise NotImplementedError()

	def load(self, filePath):
		raise NotImplementedError()

	def drop(self):
		raise NotImplementedError()


class AbstractDAO( object ):
	
	def __init(self):
		"""
		Always override the constructor, even if the constructor has no arguments.
		"""
		raise NotImplementedError()
	
	def save(self):
		raise NotImplementedError()
	
	def read(self, id):
		raise NotImplementedError()
	
	def list(self):
		raise NotImplementedError()


class AbstractVersionDAO( AbstractDAO ):
	
	def __init(self):
		"""
		Always override the constructor, even if the constructor has no arguments.
		"""
		raise NotImplementedError()
	
	def listVersionsByPath(self, requestedPathId, requestedBranchId):
		raise NotImplementedError()		

	def listBranchesByPath( self, path ):
		raise NotImplementedError()


class AbstractDAOFactory( object ):
	
	def __init__(self):
		"""
		Always override the constructor, even if the constructor has no arguments.
		"""
		raise NotImplementedError()

	def getForkDAO(self, fork):
		raise NotImplementedError()
	
	def getProjectDAO(self, project):
		raise NotImplementedError()
	
	def getBranchDAO(self, branch):
		raise NotImplementedError()

	def getFileDAO(self, path):
		raise NotImplementedError()

	def getVersionDAO(self, version):
		raise NotImplementedError()
	
	def getContributorDAO(self, contributor):
		raise NotImplementedError()
	
	def getContributionDAO(self, contribution):
		raise NotImplementedError()
