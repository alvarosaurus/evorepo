import hashlib
import os


class Fork(object):
    """
    Represents fork of at least 2 projects
    """
    def __init__(self, id=None, description=None):
        self.id = id
        self.description = description


class Project(object):
    """
    Represents a forked project
    """
    def __init__(self, id=None, name=None, forkId=None):
        self.id = id
        self.name = name
        self.forkId = forkId


class Branch(object):
    """
    Represents a branch of a project.
    """
    def __init__(self, id=0, projectId=0, name=None):
        self.id = id
        self.name = name
        self.projectId = projectId


class File(object):
    """
    Represents a file, identified by its path, across branches.
    For example: a file with the path src/main.c
    can be present in many branches,
    but will be represented by a single File object.
    """
    def __init__(self, id=None, path=None):
        self.id = id
        self.path = path


class Version(object):
    """
    Represents a version of a file in a branch
    For example: a file with the path src/main.c
    can be present in many branches,
    and will be represented by one Version object for each branch.
    """
    def __init__(self, id=None, fileId=None, branchId=None, checksum=None, size=None):
        self.id = id
        self.fileId = fileId
        self.branchId = branchId
        self.checksum = checksum
        self.size = size

    @classmethod
    def getChecksum(self, fullPath):
        hashmd5 = hashlib.md5()
        try:
            with open(fullPath, "rb") as handle:
                for chunk in iter(lambda: handle.read(4096), b""):
                    hashmd5.update(chunk)
            return hashmd5.hexdigest()

        except Exception:
            print("skipped %s" % fullPath)

    @classmethod
    def getSize(self, fullPath):
        """
        Count the number of lines in a text file

        @param fullPath string path to the file to process
        @return int or None if not a text file
        """
        try:
            with open(fullPath) as f:
                count = sum(1 for line in f)
            return count

        except UnicodeDecodeError:
            # file is not a text file
            return None
        except Exception:
            print("skipped %s" % fullPath)


class Contributor(object):
    """
    Represents a person working within the project 
    by contributing edits or commits.
    """
    def __init__(self, id=None, nickname=None):
        self.id = id
        self.nickname = nickname

    @classmethod
    def getNameChecksum(self, nickname):
        hashmd5 = hashlib.md5()
        hashmd5.update(nickname.encode("latin-1"))
        return hashmd5.hexdigest()


class Contribution(object):
    """
    Holds the count of edits or commits for this person and branch.
    """
    EDIT = 'EDIT'
    COMMIT = 'COMMIT'

    def __init__(self, id=None, contributorId=None, branchId=None, total=0, type=None):
        self.id = id
        self.contributorId = contributorId
        self.branchId = branchId
        self.total = total
        self.type = type
