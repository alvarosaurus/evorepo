
CREATE TABLE Fork (
    id          SERIAL PRIMARY KEY NOT NULL,
    description varchar(255) NOT NULL,
    constraint  fork_1 unique( description )    
);


CREATE TABLE Project (
    id          SERIAL PRIMARY KEY NOT NULL,
    name        varchar(255) NOT NULL,
    forkId      int REFERENCES Fork( id ) NOT NULL,
    constraint  project_1 unique( name )    
);

CREATE TABLE Branch (
    id          SERIAL PRIMARY KEY NOT NULL,
    name        varchar(255) NOT NULL,
    projectId   int REFERENCES Project( id ) NOT NULL,
    constraint  branch_1 unique( projectId, name )
);

CREATE TABLE File (
    id          SERIAL PRIMARY KEY NOT NULL,
    path        text NOT NULL,
    constraint  file_1 unique( path )
);
create index file_path on File(path);

CREATE TABLE Version (
    id          SERIAL PRIMARY KEY NOT NULL,
    checksum    varchar,
    fileId      int REFERENCES File( id ) NOT NULL,
    branchId    int REFERENCES Branch( id ) NOT NULL,
    constraint  fileversion_1 unique( branchId, fileId ),
    size        int
);
create index version_fileId on Version(fileId);
create index version_branchId on Version(branchId);

CREATE TABLE Contributor (
    id          SERIAL PRIMARY KEY NOT NULL,
    nickname    varchar(255) NOT NULL,
    constraint  contributor_1 unique( nickname )
);
create index contributor_nickname on Contributor(nickname);

CREATE TYPE CtbnType AS ENUM ('EDIT', 'COMMIT');

CREATE TABLE Contribution (
    id              SERIAL PRIMARY KEY NOT NULL,
    total           int,
    contributorId   int REFERENCES Contributor( id ) NOT NULL,
    branchId        int REFERENCES Branch( id ) NOT NULL,
    type            CtbnType
);
create unique index contribution_contributorbranch on Contribution(contributorId, branchId, type);
create index contribution_contributorId on Contribution(contributorId);
create index contribution_branchId on Contribution(branchId);
