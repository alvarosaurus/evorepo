.PHONY: clone createdb collect preview tree test clean _store _metrics _matrix

## get variables from the config.ini file created by ./configure
include config.ini
export $(shell sed 's/=.*//' config.ini)

## List releases (tags) from repository, apply filter
$(eval RELEASES := $(shell cd ${CLONE_PATH} && git tag | grep -vi ${FILTER}))
export RELEASES

########################
#
# Installation
#
########################

## Install required Python and R packages
install:
	cd install && \
	sudo pip3 install -r requirements.txt
	sudo Rscript -e 'install.packages(c("ape", "cluster", "ggplot2", "phangorn", "R6", "StatMatch", "testthat", "XML", "pander"), repos="https://cran.uni-muenster.de/")'

## Create a postgres user for the current user
## Create database DBNAME (set in config.ini)
createdb:
	sudo -u postgres createuser -s ${USER} || :
	createdb ${DBNAME}
	psql ${DBNAME} -f ${DB_CREATE_FILE}
	echo "Database created"

## Create directory DATADIR and clone repository GITSRC in it (set in config.ini)
clone:
	mkdir -p ${DATADIR}
	git clone ${GITSRC} ${CLONE_PATH}

###############################
#
# Collect data from the cloned repository and
# export it to a spreadsheet
#
###############################

store:
	export PYTHONPATH=${PYTHONPATH}:/src/DataCollection
	$(eval FORKNAME := ${REPONAME})
	$(eval PATH := $(abspath ${CLONE_PATH}))
	$(foreach var, ${RELEASES}, $(eval BRANCHNAME := ${var}) $(collect_cmd);)
	echo "Repository data stored in database"

metrics: store
	export PYTHONPATH=${PYTHONPATH}:src/DataCollection
	$(eval FORKNAME := "${REPONAME}")
	$(eval PATH := $(abspath ${CLONE_PATH}))
	$(eval STARTTAG := '')
	$(foreach var, ${RELEASES}, $(eval ENDTAG := ${var}) $(metrics_cmd) $(eval STARTTAG := ${var});)
	echo "Computed metrics stored in the database"	

export:${CSV_FILE}
	echo "Data collected was exported to ${CSV_FILE}."

${CSV_FILE}:
	export PYTHONPATH=${PYTHONPATH}:src/DataCollection
	$(eval FORKNAME := "${REPONAME}")
	$(eval EXPORTTYPE := 1)
	$(eval SPREADSHEET := $(abspath ${CSV_FILE}))
	$(export_cmd)	

########################
#
# Unit tests
#
########################

test:
	echo "Testing Python scripts, with test coverage."
	export PYTHONPATH=${PYTHONPATH}:src/DataCollection:test/DataCollection && \
	nosetests --with-coverage --cover-package=src/DataCollection/evorepo --cover-erase \
	test/DataCollection/MetricsTest.py \
	test/DataCollection/DatabaseTest.py \
	test/DataCollection/RepositoryTest.py \
	test/DataCollection/SpreadsheetTest.py

	echo "Testing R scripts, without test coverage."
	/usr/bin/Rscript test/DataAnalysis/all.R

###################################################
#
# Erase exported data, report, matrix, tree files.
# Do not erase the database and the repositories.
#
###################################################

cleanup:
	rm -f ${CSV_FILE}
	rm -f ${REPORT_FILE}
	rm -f ${MATRIX_FILE}
	rm -f ${TREEOBJ_FILE}
	rm -f ${TREE_FILE}
	rm -f ${HISTO_FILE}

########################
#
# Command definitions
#
########################

define collect_cmd
	cd ${PATH} && /usr/bin/git checkout -f ${BRANCHNAME}; /usr/bin/git clean -fdx
	/usr/bin/python3 src/DataCollection/collect.py \
		${FORKNAME} ${REPONAME} ${BRANCHNAME} ${PATH} ${DBNAME} 
endef

define metrics_cmd
	/usr/bin/python3 src/DataCollection/addmetrics.py \
		${REPONAME} ${STARTTAG} ${ENDTAG} ${PATH} ${DBNAME} 
endef

define export_cmd
	/usr/bin/python3 src/DataCollection/export.py \
		${DBNAME} ${FORKNAME} ${SPREADSHEET} ${EXPORTTYPE}
endef
