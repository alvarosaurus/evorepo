# evorepo
Tools for constructing phylogenetic trees of software repositories.

## Requirements
* Python 3
* PostgreSQL
* Git
* R
* XML

e.g. using Debian:
```
sudo apt-get update
sudo apt-get install python3 virtualenv postgresql python3-psycopg2 libpq-dev git r-recommended libxml2 libxml2-dev
sudo apt-get upgrade python3-setuptools
```

## Configuration
Run ```./configure``` to create a configuration file.
If you accept the defaults, this file should be suitable for building the example discussed below.
The configuration will be saved to ```config.ini``` in the working directory.
```
chmod +x configure
./configure
```

## Installation
* Install required Python packages:
* Install required R packages:
```
make install
```
(Alternatively, you may want to install these in a [virtual environment](https://docs.python.org/3/tutorial/venv.html))

You will need to create a new database to hold data and metrics about the repository.
```
make createdb
```

## Tests
To run the test do:
```
make test
```

## Example
The example builds a phylogenetic tree of Linux kernel releases.

### Collect data
Collecting the data can take several hours. On my 2GHz, 2GB RAM Linux machine, it took more than 5 hours.

First you'll need a repository to work on. In this example, I cloned the Linux kernel repository.
The repository's git address is stored in the variable GITSRC in the file ```config.ini```.
By default, the repository is cloned to the ```Data``` directory.
```
make clone
```

Data about the repository is stored in the database: branches, files, file versions, edits and commits;
software metrics are collected for each release: presence/absence and checksums of versions of files,
presence/absence of developers in a release team, number of edits and commits per developer.
Developers are identified by unique, non-reversible cryptographic hashes (md5).
```
make metrics
```
Export the metrics to a spreadsheet that can be used to analyse the data.
The spreadsheet is saved by default to ```Data/Linux.csv```, a report with counts of what was
exported is saved to ```Data/Linux_report.txt```.

```
make export
```

### Analyse data
Once the data about the repository has been exported to ```Data/Linux.csv```,
you can analyse it by computing a matrix of the pairwise distance between branches.
The matrix will be saved by default to ```Data/Linux_matrix.RData```.
```
./evorepo matrix
```

Now you can estimate a phylogenetic tree from the distance matrix.
The tree object will be saved by default to ```Data/Linux_tree.RData```
```
./evorepo tree
```

The tree object can now be visualized.
The plotted image of the tree will be saved by default to ```Data/Linux_tree.png``` (fig. 1).
```
./evorepo plot
```

![linux_all_tree](https://user-images.githubusercontent.com/679068/34471269-f7406590-ef44-11e7-8415-e0874f4b38d7.png)

*__Figure 1:__ A phylogenetic tree of the Linux kernel.*

Some insight about the evolution of the project can be gained by examining the distances
between branches of the tree (fig. 2). Statistics are computed from square-root-transformed data,
as this transformation gives a distribution closer to normal than the untransformed data.
The histogram is saved by default to ```Data/Linux_hist.png```.
```
./evorepo histo
```

![linux_all_hist](https://user-images.githubusercontent.com/679068/34471452-81c35588-ef4a-11e7-8211-c8608501fbf3.png)

*__Figure 2:__ Basic statistics about the tree.*


The evolution of the project can be examined further by generating a matrix of the distances
between the branches of the tree. A matrix of distances can be used
to detect outliers. The histogram (fig. 2) uses square-root-transformed data, so the values
of the 95% interval computed above need to be squared to apply them to untransformed distances.
Therefore, branch distances below 0.011 and above 0.447 can be considered outliers.

The matrix below (Tab. 1) shows a small part of the cophenetic distance matrix for
the Linux kernel. It shows some of the largest distances in the matrix, which are above the 0.447
threshold.
The matrix is saved by default to ```Data/Linux_cophenetic.Rmd```, in Rmarkdown format.
```
./evorepo cophenetic
```
*__Table 1:__ A (small) part of the cophenetic distance matrix.*

|   &nbsp;    |  v4.2   |  v4.3   |  v4.4   |  v4.5   |  v4.6   |  v4.7   |
|:-----------:|:-------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| **v2.6.12** | 0.4419  | 0.4524  | 0.4629  | 0.4806  | 0.4911  | 0.4963  |
| **v2.6.39** | 0.3954  | 0.4059  | 0.4164  | 0.4342  | 0.4447  | 0.4499  |
|  **v3.0**   | 0.3006  | 0.3111  | 0.3216  | 0.3394  | 0.3499  |  0.355  |
|  **v3.1**   | 0.2916  | 0.3021  | 0.3126  | 0.3304  | 0.3409  | 0.3461  |
|  **v3.10**  | 0.2726  | 0.2831  | 0.2936  | 0.3114  | 0.3219  | 0.3271  |
|  **v3.11**  | 0.2626  | 0.2731  | 0.2836  | 0.3014  | 0.3119  | 0.3171  |


## Further work

This code was written as part of my [master in software engineering](https://github.com/alvarosaurus/master_dissertation).
